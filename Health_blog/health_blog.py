from flask import Flask
from flask import render_template

app = Flask(__name__)


# temp data for posts

posts = [
    {
        'author': "Jamil",
        'title': "one",
        'content': "soup",
        'date_posted': "today"
    },

    {
        'author': "Lingfe",
        'title': "two",
        'content': "bread",
        'date_posted': "now"
    }
]



@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', posts=posts)

@app.route("/about")
def about():
    return render_template('about.html')


if __name__ == '__main__':
    app.run(debug=True)
