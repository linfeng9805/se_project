# Store this code in 'app.py' file

from flask import Flask, render_template, jsonify, request, redirect, url_for, session
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re

app = Flask(__name__)


app.secret_key = '\xd3m\x8f"\x19#\xad\xb4QL4\xb4\r\xf4\xc7\x86\xfcK\xa2\x12\xb0iP\x99'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'Horizon0'
app.config['MYSQL_DB'] = 'HospitalManage'

mysql = MySQL(app)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/patlogin', methods =['GET', 'POST'])
def plogin():
	msg = ''
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
		username = request.form['username']
		password = request.form['password']
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM patient WHERE username = % s AND password = % s', (username, password, ))
		patient = cursor.fetchone()
		if patient:
			session['loggedin'] = True
			session['id'] = patient['pid']
			session['username'] = patient['username']
			msg = 'Logged in successfully !'
			return render_template('patient/index.html', msg = msg)
		else:
			msg = 'Incorrect username / password !'
	return render_template('patient/login.html', msg = msg)

@app.route('/patlogout')
def plogout():
	session.pop('loggedin', None)
	session.pop('id', None)
	session.pop('username', None)
	return redirect(url_for('plogin'))

@app.route('/patregister', methods =['GET', 'POST'])
def pregister():
	msg = ''
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form \
			and 'ppassword' in request.form and 'pemail' in request.form and 'phone' in request.form and 'street' in request.form \
			and 'city' in request.form:
		username = request.form['username']
		password = request.form['password']
		ppassword = request.form['ppassword']
		email = request.form['email']
		pemail = request.form['pemail']

		phone = request.form['phone']
		street = request.form['street']
		city = request.form['city']
		country = request.form['country']
		gender = request.form['gender']
		nationality = request.form['nationality']
		children = request.form['children']

		year = request.form['year']
		month = request.form['month']
		day = request.form['day']
		birthday = day+month+year

		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM patient WHERE username = % s', (username, ))
		patient = cursor.fetchone()
		if patient:
			msg = 'patient already exists !'
		elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
			msg = 'Invalid email address !'
		elif not re.match(r'[A-Za-z0-9]+', username):
			msg = 'Username must contain only characters and numbers !'
		elif not re.match(ppassword, password):
			msg = 'retype password !'
		elif not re.match(pemail, email):
			msg = 'retype email !'
		elif not username or not password or not email or not phone or not street or not city:
			msg = 'Please fill out the form !'
		else:
			cursor.execute('INSERT INTO patient VALUES (NULL, % s, % s, % s, %s, %s, %s, %s, %s, %s, %s, %s)', (username, password, email, phone, street, city, country, gender, birthday, nationality, children))
			mysql.connection.commit()
			msg = 'You have successfully registered !'
	elif request.method == 'POST':
		msg = 'Please fill out the form !'
	return render_template('patient/register.html', msg = msg)

@app.route('/doclogin', methods =['GET', 'POST'])
def dlogin():
	msg = ''
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
		username = request.form['username']
		password = request.form['password']
		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM physicians WHERE username = % s AND password = % s', (username, password,))
		physicians = cursor.fetchone()
		if physicians:
			session['loggedin'] = True
			session['id'] = physicians['idphysicians']
			session['username'] = physicians['username']
			msg = 'Logged in successfully !'
			return render_template('doctor/index.html', msg = msg)
	else:
			msg = 'Incorrect username / password !'
	return render_template('doctor/login.html', msg = msg)

@app.route('/doclogout')
def dlogout():

	session.pop('loggedin', None)
	session.pop('id', None)
	session.pop('username', None)
	return redirect(url_for('dlogin'))

@app.route('/docregister', methods =['GET', 'POST'])
def dregister():
	msg = ''
	if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form \
			and 'ppassword' in request.form and 'pemail' in request.form and 'phone' in request.form and 'street' in request.form \
			and 'city' in request.form:
		username = request.form['username']
		password = request.form['password']
		ppassword = request.form['ppassword']
		email = request.form['email']
		pemail = request.form['pemail']

		phone = request.form['phone']
		street = request.form['street']
		city = request.form['city']
		country = request.form['country']
		gender = request.form['gender']
		types = request.form['types']
		specialization = request.form['specialization']
		classification = request.form['classification']

		year = request.form['year']
		month = request.form['month']
		day = request.form['day']
		birthday = day + month + year

		cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
		cursor.execute('SELECT * FROM physicians WHERE username = % s', (username,))
		physicians = cursor.fetchone()
		if physicians:
			msg = 'physicians already exists !'
		elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
			msg = 'Invalid email address !'
		elif not re.match(r'[A-Za-z0-9]+', username):
			msg = 'Username must contain only characters and numbers !'
		elif not re.match(ppassword, password):
			msg = 'retype password !'
		elif not re.match(pemail, email):
			msg = 'retype email !'
		elif not username or not password or not email or not phone or not street or not city:
			msg = 'Please fill out the form !'
		else:
			cursor.execute('INSERT INTO physicians VALUES (NULL, % s, % s, % s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 0)', (
				username, password, email, phone, street, city, country, gender, birthday, types, specialization,
				classification))
			mysql.connection.commit()
			msg = 'You have successfully registered !'
	elif request.method == 'POST':
		msg = 'Please fill out the form !'
	return render_template('doctor/register.html', msg = msg)
